/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.Controller;

import com.parcial.demo.metodo.Departamento;
import com.parcial.demo.metodo.Empresa;
import com.parcial.demo.metodo.interface_service.IdepartamentoService;
import com.parcial.demo.metodo.interface_service.IempresaService;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author yamil
 */
@Controller
@RequestMapping
public class DepartamentoController {
    
    @Autowired
    private IdepartamentoService service_depa;
    
    @Autowired
    private IempresaService service_emp;
        
   
    @GetMapping("/listardepartamento")
    public String Listar(Model model){
        List<Departamento> departamento=service_depa.listar();
        model.addAttribute("departamento", departamento);
        return "listardepartamento";
    }
    
    
     @GetMapping("/listarIddepartamento")
    public String listarId(@PathVariable int idDepartamento ,Model model){
        model.addAttribute("departamento", service_depa.listarId(idDepartamento));
        
        List<Empresa> empresa=service_emp.listar();        
        model.addAttribute("empresa", empresa);
        return "formdepartamento";
    }
    
    @GetMapping("/newdepartamento")
    public String agregar(Model model){
        model.addAttribute("departamento", new Departamento());
        return "formdepartamento";
    }
    
    @PostMapping("/savedepartamento")
    public String save(@Valid Departamento p, Model model){
        service_depa.save(p);
        return "redirect:/listardepartamento";
    }
    
    @GetMapping("/eliminardepartamento/{idDepartamento}")
    public String delete (@PathVariable int idDepartamento, Model model){
        service_depa.delete(idDepartamento);
        return "redirect:/listardepartamento";
    }
    
    @GetMapping("/editardepartamento/{idDepartamento}")
    public String editar(@PathVariable int idDepartamento, Model model){
        Optional<Departamento>departamento=service_depa.listarId(idDepartamento);
        model.addAttribute("departamento", departamento);
        
        return "formdepartamento";
    }
}
