/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.Controller;

import com.parcial.demo.metodo.Empresa;
import com.parcial.demo.metodo.interface_service.IempresaService;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author yamil
 */
@Controller
@RequestMapping
public class EmpresaController {
    
    @Autowired
    private IempresaService service;
    
    @RequestMapping("/index")
    public String page(Model model) {
        model.addAttribute("attribute", "value");
        return "index";
    }
    
     @GetMapping("/listarempresa")
    public String Listar(Model model){
        List<Empresa> empresa=service.listar();
        model.addAttribute("empresa", empresa);
        return "listarempresa";
    }
    
    @GetMapping("/listarIdempresa")
    public String listarId(@PathVariable int idEmpresa ,Model model){
        model.addAttribute("empresa", service.listarId(idEmpresa));
        return "formempresa";
    }
    
    @GetMapping("/newempresa")
    public String agregar(Model model){
        model.addAttribute("empresa", new Empresa());
        return "formempresa";
    }
    
    @PostMapping("/saveempresa")
    public String save(@Valid Empresa p, Model model){
        service.save(p);
        return "redirect:/listarempresa";
    }
    
    @GetMapping("/eliminarempresa/{idEmpresa}")
    public String delete (@PathVariable int idEmpresa, Model model){
        service.delete(idEmpresa);
        return "redirect:/listarempresa";
    }
    
    @GetMapping("/editarempresa/{idEmpresa}")
    public String editar(@PathVariable int idEmpresa, Model model){
        Optional<Empresa>empresa=service.listarId(idEmpresa);
        model.addAttribute("empresa", empresa);
        return "formempresa";
    }
    
}
