/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.Controller;

import com.parcial.demo.metodo.Departamento;
import com.parcial.demo.metodo.Empleados;
import com.parcial.demo.metodo.interface_service.IdepartamentoService;
import com.parcial.demo.metodo.interface_service.IempleadosService;
import java.util.List;
import java.util.Optional;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author yamil
 */
@Controller
@RequestMapping
public class EmpleadosController {
    
    @Autowired
    private IempleadosService service_emp;
    
    @Autowired
    private IdepartamentoService service_depa;
    
    
     @GetMapping("/listarempleados")
    public String Listar(Model model){
        List<Empleados> empleados=service_emp.listar();
        model.addAttribute("empleados", empleados);
        return "listarempleados";
    }
    
    
    
      @GetMapping("/listarIdempleados")
    public String listarId(@PathVariable int idEmpleados ,Model model){
        model.addAttribute("emplados", service_emp.listarId(idEmpleados));
        
        List<Departamento> departamento=service_depa.listar();        
        model.addAttribute("departamento", departamento);
        return "formempleados";
    }
    
      @GetMapping("/newempleados")
    public String agregar(Model model){
        model.addAttribute("empleados", new Empleados());
        return "formempleados";
    }
    
    @PostMapping("/saveempleados")
    public String save(@Valid Empleados p, Model model){
        service_emp.save(p);
        return "redirect:/listarempleados";
    }
    
    
    @GetMapping("/eliminarempleados/{idEmpleado}")
    public String delete (@PathVariable int idEmpleado, Model model){
        service_emp.delete(idEmpleado);
        return "redirect:/listarempleados";
    }
    
    @GetMapping("/editarempleados/{idEmpleado}")
    public String editar(@PathVariable int idEmpleado, Model model){
        Optional<Empleados>empleados=service_emp.listarId(idEmpleado);
        model.addAttribute("empleados", empleados);
        
        return "formempleados";
    }
}

