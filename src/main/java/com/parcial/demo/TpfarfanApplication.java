package com.parcial.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpfarfanApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpfarfanApplication.class, args);
	}

}
