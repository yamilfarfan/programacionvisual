package com.parcial.demo.metodo.interface_em_dep_emp;


import com.parcial.demo.metodo.Departamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author yamil
 */
@Repository
public interface IdepartamentoRepo extends CrudRepository<Departamento, Integer>{
    
}
