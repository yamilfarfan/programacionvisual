/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.metodo.interface_em_dep_emp;

import com.parcial.demo.metodo.Empleados;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author yamil
 */
@Repository
public interface IempleadosRepo extends CrudRepository<Empleados, Integer>{
    
}
