/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.metodo.interface_service;

import com.parcial.demo.metodo.Departamento;
import java.util.List;
import java.util.Optional;
import org.springframework.ui.Model;

/**
 *
 * @author yamil
 */
public interface IdepartamentoService {
    public List<Departamento>listar();
    public Optional<Departamento> listarId(int  idDepartamento);
    public int save(Departamento p);
    public void delete(int  idDepartamento);
}
