/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.metodo.interface_service;

import com.parcial.demo.metodo.Empresa;
import com.parcial.demo.metodo.Empresa;
import java.util.List;
import java.util.Optional;

/**
 *
 * @author yamil
 */
public interface IempresaService {
   public List<Empresa>listar();
    public Optional<Empresa> listarId(int idEmpresa);
    public int save(Empresa p);
    public void delete(int idEmpresa);
  
}
