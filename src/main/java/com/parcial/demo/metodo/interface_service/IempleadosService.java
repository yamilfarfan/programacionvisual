/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.metodo.interface_service;

import com.parcial.demo.metodo.Empleados;
import java.util.List;
import java.util.Optional;
import org.springframework.ui.Model;

/**
 *
 * @author yamil
 */
public interface IempleadosService {
    public List<Empleados>listar();
    public Optional<Empleados> listarId(int idEmpleado);
    public int save(Empleados p);
    public void delete(int idEmpleado);
   
}
