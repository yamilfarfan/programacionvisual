/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.metodo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import static org.apache.tomcat.jni.Lock.name;

/**
 *
 * @author yamil
 */
@Entity
@Table (name="empleados")
public class Empleados {
   @Id
    /*@GeneratedValue (strategy = GenerationType.SEQUENCE)*/
    @Column(name="idEmpleado")
    private int idEmpleado;
     
    
    @Column(name = "apellido")
    private String apellido;
      
   
    @Column(name = "nombres")
    private String nombres;
      
    @Column(name="dni")
    private int dni;
    
     @Column(name="cuil")
    private int cuil;
     
     @Column(name="direccion")
    private String direccion;
     
     @Column(name="telefono")
    private int telefono;
     
    @Column(name="email")
    private String email;
            
    @Column(name="usuario")
    private String usuario;
    
    @Column(name="password")
    private String password;
    
    @Column(name="estado")
    private String estado;
    
    @ManyToOne
    @JoinColumn(name="idDepartamento")
    private Departamento idDepartamento;
    
    public Empleados() {
    }  

    public Empleados(int idEmpleado, String apellido, String nombres, int dni, int cuil, String direccion, int telefono, String email, String usuario, String password, String estado, Departamento idDepartamento) {
        this.idEmpleado = idEmpleado;
        this.apellido = apellido;
        this.nombres = nombres;
        this.dni = dni;
        this.cuil = cuil;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.usuario = usuario;
        this.password = password;
        this.estado = estado;
        this.idDepartamento = idDepartamento;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public int getDni() {
        return dni;
    }

    public void setDni(int dni) {
        this.dni = dni;
    }

    public int getCuil() {
        return cuil;
    }

    public void setCuil(int cuil) {
        this.cuil = cuil;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Departamento getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(Departamento idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    
    
    
    
    
}