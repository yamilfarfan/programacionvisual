/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.metodo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author yamil
 */
@Entity
@Table (name="departamento")
public class Departamento {
    @Id
    /*@GeneratedValue (strategy = GenerationType.SEQUENCE)*/
    @Column(name="idDepartamento")
    private int idDepartamento;
    
    @Column(name="nombreDto")
    private String nombreDto;
    
    @ManyToOne
    @JoinColumn(name="idEmpresa")
    private Empresa idEmpresa;
    
   
     public Departamento() {
    } 

    public Departamento(int idDepartamento, String nombreDto, Empresa idEmpresa) {
        this.idDepartamento = idDepartamento;
        this.nombreDto = nombreDto;
        this.idEmpresa = idEmpresa;
    }

    public int getIdDepartamento() {
        return idDepartamento;
    }

    public void setIdDepartamento(int idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public String getNombreDto() {
        return nombreDto;
    }

    public void setNombreDto(String nombreDto) {
        this.nombreDto = nombreDto;
    }

    public Empresa getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Empresa idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    
     
     
}


