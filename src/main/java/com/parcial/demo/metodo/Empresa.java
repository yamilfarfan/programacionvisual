/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.metodo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author yamil
 */
@Entity
@Table (name="empresa")
public class Empresa {
    @Id
    /*@GeneratedValue (strategy = GenerationType.SEQUENCE)*/
    @Column(name="idEmpresa") 
    private int idEmpresa;

    @Column(name="nombreEmpresa")
    private String nombreEmpresa;
    
    @Column(name="direccion")
    private String direccion;
    
    @Column(name="ciudad")
    private String ciudad;
     
    @Column(name="codigopostal")
    private int codigopostal;
    
    @Column(name="telefono")
    private int telefono;
     
    @Column(name="email")
    private String email;
    
    public Empresa() {
        
    }

    public Empresa(int idEmpresa, String nombreEmpresa, String direccion, String ciudad, int codigopostal, int telefono, String email) {
        this.idEmpresa = idEmpresa;
        this.nombreEmpresa = nombreEmpresa;
        this.direccion = direccion;
        this.ciudad = ciudad;
        this.codigopostal = codigopostal;
        this.telefono = telefono;
        this.email = email;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getCodigopostal() {
        return codigopostal;
    }

    public void setCodigopostal(int codigopostal) {
        this.codigopostal = codigopostal;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    
    
}

  
   
