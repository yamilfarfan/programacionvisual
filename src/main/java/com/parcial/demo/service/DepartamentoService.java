/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.service;

import com.parcial.demo.metodo.Departamento;
import com.parcial.demo.metodo.interface_em_dep_emp.IdepartamentoRepo;
import com.parcial.demo.metodo.interface_service.IdepartamentoService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author yamil
 */
@Service
public class DepartamentoService implements IdepartamentoService{
    
    @Autowired
    private IdepartamentoRepo data;
    
    @Override
    public List<Departamento> listar() {
        return (List<Departamento>) data.findAll();
    }

    @Override
    public Optional<Departamento> listarId(int idDepartamento) {
        return data.findById(idDepartamento);
    }

    @Override
    public int save(Departamento p) {
        int res=0;
        Departamento Departamento = data.save(p);
        if (!Departamento.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void delete(int idDepartamento) {
        data.deleteById(idDepartamento);
    }
}
