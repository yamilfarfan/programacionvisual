/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.service;

import com.parcial.demo.metodo.Empleados;
import com.parcial.demo.metodo.interface_em_dep_emp.IempleadosRepo;
import com.parcial.demo.metodo.interface_service.IempleadosService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author yamil
 */
@Service
public class EmpleadosService implements IempleadosService {
    
   @Autowired   
   private IempleadosRepo data;
   
   @Override
    public List<Empleados> listar() {
        return (List<Empleados>) data.findAll();
    }

    @Override
    public Optional<Empleados> listarId(int idEmpleado) {
        return data.findById(idEmpleado);
    }

    @Override
    public int save(Empleados p) {
        int res=0;
        Empleados Empleados = data.save(p);
        if (!Empleados.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void delete(int idEmpleado) {
        data.deleteById(idEmpleado);
    }
   
   
   
}
