/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.parcial.demo.service;

import com.parcial.demo.metodo.Empresa;
import com.parcial.demo.metodo.interface_em_dep_emp.IempresaRepo;
import com.parcial.demo.metodo.interface_service.IempresaService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author yamil
 */
@Service
public class EmpresaService implements IempresaService{

    @Autowired
    private IempresaRepo data;

    @Override
    public List<Empresa> listar() {
        return (List<Empresa>) data.findAll();
    }
    
    @Override
    public Optional<Empresa> listarId(int idEmpresa){
        return data.findById(idEmpresa);
    }

    @Override
    public int save(Empresa p) {
        int res=0;
        Empresa Empresa = data.save(p);
        if (!Empresa.equals(null)) {
            res = 1;
        }
        return res;
    }

    @Override
    public void delete(int idEmpresa) {
        data.deleteById(idEmpresa);
    }
    
    
    
    
    
    
    
    

}
